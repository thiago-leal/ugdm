#!/usr/bin/env node
const fs = require("fs");
const path = require("path");

const package = fs.readFileSync(path.join(__dirname, "package.json")).toString();
const version = fs.readFileSync(path.join(__dirname, "version.txt")).toString().trim();
const json = JSON.parse(package);

json.version = version;

fs.writeFileSync(path.join(__dirname, "package.json"), JSON.stringify(json, null, 2));
