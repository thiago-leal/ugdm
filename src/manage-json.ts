import { existsSync, readFileSync, writeFileSync } from "fs";
import { join } from "path";

import { jsonFileName } from "./const";
import { JSONFileDeclaration } from "./types";

function removeEmptyKeys<T>(obj: T): T {
  if (typeof obj === "object") {
    const ret = Object.create(null);

    for (const key in obj) {
      if (obj[key]) {
        ret[key] = obj[key];
      }
    }
    return ret;
  }
  return obj;
}

export class ManageJSON {
  private _path: string;
  private _file: string;
  private _fileName: string;
  private _content: JSONFileDeclaration;


  get path(): string {
    return this._path;
  }

  get file(): string {
    return this._file;
  }

  get fileName(): string {
    return this._fileName;
  }

  get content(): JSONFileDeclaration {
    return { ...this._content };
  }

  /**
   * Manage JSON config file
   */
  constructor(path: string) {
    this._path = path || __dirname;
    this._file = jsonFileName;
    this._fileName = join(this._path, this._file);
    this._content = {
      source: "vendor/"
    };
  }

  /**
   * Loads file content
   */
  load(): this {
    if (existsSync(this._fileName)) {
      const content = readFileSync(this._fileName);
      this._content = JSON.parse(content.toString())
    }
    return this;
  }

  /**
   * Save file contents
   */
  save() {
    writeFileSync(this._fileName, JSON.stringify(this._content, null, 2));
  }

  /**
   * Inits a config file
   */
  init(source: string): void {
    this._content.source = source;
  }

  /**
   * Set a dependency package config
   */
  setDependency({ name, url, auth, version, commit, commands, run }: {
    name:       string
    url:        string
    version:    string
    auth?:      string
    commit?:    string
    commands?:  string | Record<string, string>
    run?:       string | string[]
  }) {
    if (!this._content.dependencies) {
      this._content.dependencies = Object.create(null);
    }
    this._content.dependencies![name] = removeEmptyKeys({
      url, auth, version, commit, commands, run
    });
  }

  /**
   * Remove a dependency package config
   */
  removeDependency(name: string): void {
    if (this._content.dependencies) {
      delete this._content.dependencies[name];
    }
  }

  /**
   * Set a script variable
   */
  setVariable(name: string, value: string | number | boolean) {
    if (!this._content["variables"]) this._content.variables = {};
    this._content.variables[name] = value;
  }

  /**
   * Remove a script variable
   */
  unsetVariable(name: string) {
    if (this._content["variables"]) {
      if (this._content.variables[name]) {
        delete this._content.variables[name];
      }
      if (!Object.keys(this._content.variables).length) {
        delete this._content.variables;
      }
    }
  }

  /**
   * Set a script scope
   */
  setScope(name: string, url: string, auth?: string) {
    if (!this._content["scopes"]) this._content.scopes = {};

    if (auth) {
      this._content.scopes[name] = { url, auth };
    } else {
      this._content.scopes[name] = url;
    }
  }

  /**
   * Remove a script scope
   */
  unsetScope(name: string) {
    if (this._content["scopes"]) {
      if (this._content.scopes[name]) {
        delete this._content.scopes[name];
      }
      if (!Object.keys(this._content.scopes).length) {
        delete this._content.scopes;
      }
    }
  }

  getScope(name: string): string | undefined {
    const scope = this._content.scopes?.[name];

    if (typeof scope === "string") return scope;
    return scope?.url;
  }

  /**
   * Get path to dependencies directory
   */
  getSourcePath(): string {
    return this._content ? this._content.source : "";
  }
}
