#!/usr/bin/env node
import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { init, add, remove, build, install, update } from "./modules";
import { setVariable, unsetVariable } from "./modules/variables";
import { setScope, unsetScope } from "./modules/scopes";

yargs(hideBin(process.argv))
	.scriptName("ugdm")
	.usage("$0 <cmd> [args]")
	.command("init", "Initialize GIT dependencies", {
		path: {
			alias: "p",
			description: "Path to save dependencies",
			type: "string",
			default: "vendor/"
		}
	}, args => {
		init(args.path);
	})
	.command("add [name] [url]", "Add GIT dependency", {
		name: {
			description: "Package alias name",
			type: "string",
			required: true
		},
		url: {
			description: "Repository address",
			type: "string",
			required: true
		},
		ver: {
			alias: "v",
			description: "Dependency version (branch | tag | commit)",
			type: "string"
		},
		commands: {
			alias: "c",
			description: "Commands to execute after install",
			type: "string"
		},
		run: {
			alias: "r",
			description: "Scripts to run after install",
			type: "string",
			default: ""
		},
		auth: {
			alias: "a",
			description: "Authentication to access private repositories",
			type: "string"
		}
	}, args => {
		add({
			name: args.name,
			url: args.url,
			version: args.ver,
			commands: args.commands,
			run: args.run ? args.run.split(",") : void 0,
			auth: args.auth
		});
	})
	.command("remove [name]", "Remove GIT dependency", {
		name: {
			description: "Package alias name",
			type: "string",
			required: true
		}
	}, args => {
		remove(args.name);
	})
	.command("install", "Install GIT dependencies", {}, () => {
		install();
	})
	.command("update [name]", "Update dependency version", {
		name: {
			description: "Package alias name",
			type: "string",
			required: true
		},
		tag: {
			alias: "t",
			description: "Use tag",
			type: "boolean"
		},
		branch: {
			alias: "b",
			description: "Branch to get version",
			type: "string"
		}
	}, args => {
		update(args.name, args.tag, args.branch);
	})
	.command("build [os]", "Generate shell script to install dependencies", {
		os: {
			description: "OS to generate script (linux | win32)",
			type: "array"
		},
		auth: {
			alias: "a",
			description: "Use authentication",
			type: "boolean",
			default: true
		},
		"no-auth": {
			description: "Ignore authentication",
			type: "boolean",
			default: false
		},
		prod: {
			description: "Production mode",
			type: "boolean",
			default: false
		},
		dev: {
			description: "Development mode",
			type: "boolean",
			default: false
		}
	}, args => {
		build(
			args.os as string[] | undefined,
			!args.dev && (args.prod || args.auth && !args["no-auth"]),
			args.prod && !args.dev
		);
	})
	.command("set [var] [value]", "Set variable", {
		"var": {
			description: "Variable name",
			type: "string",
			required: true
		},
		value: {
			description: "Variable value",
			required: true
		}
	}, args => {
		setVariable(args["var"], args.value as string | number | boolean);
	})
	.command("unset [var]", "Unset variable", {
		"var": {
			description: "Variable name",
			type: "string",
			required: true
		}
	}, args => {
		unsetVariable(args["var"]);
	})
	.command("scope [name] [url]", "Add scope name", {
		name: {
			description: "Scope name",
			type: "string",
			required: true
		},
		url: {
			description: "Scope url",
			type: "string",
			required: true
		},
		auth: {
			alias: "a",
			description: "Authentication to access scope",
			type: "string"
		}
	}, args => {
		setScope(args.name, args.url, args.auth);
	})
	.command("unscope [name]", "Remove scope name", {
		name: {
			description: "Scope name",
			type: "string",
			required: true
		}
	}, args => {
		unsetScope(args.name);
	})
	.argv;
