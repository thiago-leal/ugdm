import * as child_process from "child_process";

/**
 * Execute a shell command
 */
export async function exec(command: string, options?: child_process.ExecOptions): Promise<{
  error: child_process.ExecException | null,
  stdout: string,
  stderr: string
}> {
  return await new Promise((resolve, reject) => {
    child_process.exec(command, {
      ...options
    }, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      } else {
        resolve({ error, stdout, stderr });
      }
    });
  });
}
