import { createLinux, createWindows } from "../manage-script";
import { PATH } from "./consts";

export async function build(types?: string[], useAuth = true, prod = false) {
  if (!types) types = [ "linux", "win32" ];
  if (!Array.isArray(types)) types = [ types ];

  if (types.includes("linux")) {
    console.log("Generating shell script...");
    await createLinux(PATH, useAuth, prod);
    console.log("Shell script generated successfully");
  }
  if (types.includes("win32")) {
    console.log("Generating batch script...");
    createWindows(PATH, useAuth, prod);
    console.log("Batch script generated successfully");
  }
}
