import { ManageJSON } from "../manage-json";

export const PATH = process.cwd();
export const jsonManager = new ManageJSON(PATH).load();