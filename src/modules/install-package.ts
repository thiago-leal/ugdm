import { existsSync } from "fs";
import { PATH, jsonManager } from "./consts";
import { exec } from "./exec";

function toJSON<T>(value: T | T[]): T[] {
  if (Array.isArray(value)) {
    return value;
  }
  return [ value ];
}

/**
 * Install package
 * 
 * @returns { Promise<string> } Installed version
 */
export async function installPackage({ name, url, version, commit, commands, run }: {
  name:       string
  url:        string
  version:    string
  commit?:    string
  commands?:  string | Record<string, string>
  run?:       string | string[]
}): Promise<string> {
  const path = `${ PATH }/${ jsonManager.getSourcePath() }/${ name }`;
  let update = false;

  if (update = existsSync(path)) {
    await exec("git reset --hard HEAD && git checkout HEAD && git fetch -tap", {
      cwd: path
    });
  } else {
    if (url.startsWith("@")) {
      const split = url.split("/");
      url = split
        .map(e => e.startsWith("@") ? jsonManager.getScope(e.slice(1)) : e)
        .join("/");
    }
    await exec(`git clone -b ${ version } ${ url } ${ name }`, {
      cwd: `${ PATH }/${ jsonManager.getSourcePath() }`
    });
  }

  const isTag = await exec(`git show-ref --verify refs/tags/${ version }`, { cwd: path })
    .then(e => e.stdout.toString() ? true : false)
    .catch(() => false);

  if (update) {
    if (isTag) {
      await exec(`git checkout HEAD && git pull && git checkout ${ version }`, {
        cwd: path
      });
    } else {
      await exec(`git checkout ${ version } && git pull`, {
        cwd: path
      });
    }
  }
  if (commit) {
    await exec(`git checkout ${ commit }`, { cwd: path });
  }
  if (isTag) {
    commit = version;
  } else {
    const ret = await exec("git rev-parse HEAD", { cwd: path });
    commit = ret.stdout.toString().trim();
  }
  if (run && run.length) {
    await Promise.all(toJSON(run).map(async e => {
      let cmd = "";

      switch (process.platform) {
        case "win32":
          cmd = `CALL ${ e }`
          break;
        case "linux":
          cmd = `sh ${ e }`;
      }
      if (cmd) {
        await exec(cmd, { cwd: path });
      }
    }));
  }
  if (commands) {
    let cmd = null;

    if (typeof commands === "string") {
      cmd = commands;
    } else if (commands[process.platform]) {
      cmd = commands[process.platform]
    }
    if (cmd) {
      await exec(cmd, { cwd: path });
    }
  }
  return commit;
}
