import { jsonManager } from "./consts";

export function setScope(name: string, url: string, auth?: string) {
  jsonManager.setScope(name, url, auth);
  jsonManager.save();
}

export function unsetScope(name: string) {
  jsonManager.unsetScope(name);
  jsonManager.save();
}
