import { jsonManager, PATH } from "./consts";
import { exec } from "./exec";

export async function remove(name: string) {
  console.log(`Removing package "${ name }"...`);
	jsonManager.removeDependency(name);
	jsonManager.save();

	try {
		await exec(`rm -rf ${ name }`, {
			cwd: `${ PATH }/${ jsonManager.getSourcePath() }`
		});
		console.log(`Package removed: "${ name }"`);
	} catch (e) {
		console.error(`Fail to remove package: "${ name }"`);
		process.exit(1);
	}
}
