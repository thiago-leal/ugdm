import { existsSync, mkdirSync } from "fs";
import { getArrayFromObject } from "../manage-script";
import { jsonManager } from "./consts";
import { installPackage } from "./install-package";

export async function install() {
  console.log("Installing dependencies...");

	const path = jsonManager.getSourcePath().split("/");

  try {
    await Promise.all(getArrayFromObject(jsonManager.content.dependencies || {}).map(async e => {
      const name = e.key;
      let { url, version, commit, commands, run, auth } = e.value;
      let mkdirPath = "";

      for (let i = 0; i < path.length; i++) {
        mkdirPath += `${ path[i] }/`;
        if (!existsSync(mkdirPath)) {
          mkdirSync(mkdirPath);
        }
      }
  		commit = await installPackage({ name, url, version, commit, commands, run });
      jsonManager.setDependency({
        name, url, version, commands, auth, run, commit: commit === version ? void 0 : commit
      });
      jsonManager.save();
      console.log(`Package installed: ${ name }:"${ url }"@${ version }`);
    }));
	} catch (e) {
		console.error(`Failed to install dependencies`);
		process.exit(1);
	}
}
