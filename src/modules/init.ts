import { jsonManager } from "./consts";

export function init(path: string) {
  console.log("Initializing Universal GIT Dependency Manager...");
  jsonManager.init(path);
  jsonManager.save();
  console.log(`Universal GIT Dependency Manager initialized in path: "${ path }"`);
}