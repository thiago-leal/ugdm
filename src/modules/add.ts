import { existsSync, mkdirSync } from "fs";
import { createInterface } from "readline";
import { jsonManager, PATH } from "./consts";
import { exec } from "./exec";
import { installPackage } from "./install-package";

function read(message: string): Promise<string> {
	const input = createInterface({
		input: process.stdin,
		output: process.stdout
	});
	return new Promise<string>(resolve => input.question(message, value => {
		resolve(value);
		input.close();
	}));
}

export async function add({ name, url, version, commands, run, auth }: {
	name:				string
	url?:				string
	version?:		string
	commands?:	string | Record<string, string>
	run?:				string | string[]
	auth?:			string
}) {
	if (!url) url = await read("Input the repository url: ");
	if (!version) version = (await read("Input the version to use [master]: ")) || "master";

  console.log(`Adding package "${ name }:'${ url }'@${ version }"...`);

	const path = jsonManager.getSourcePath().split("/");
	let mkdirPath = "";

	try {
		for (let i = 0; i < path.length; i++) {
			mkdirPath += `${ path[i] }/`;
			if (!existsSync(mkdirPath)) {
				mkdirSync(mkdirPath);
			}
		}
		const commit = await installPackage({ name, url, version, commands, run });

		jsonManager.setDependency({
			name, url, version, commands, run, auth,
			commit: commit === version ? void 0 : commit
		});
		jsonManager.save();
		console.log(`Package added: ${ name }:"${ url }"@${ version }`);
	} catch (e) {
		await exec(`rm -rf ${ name }`, {
			cwd: `${ PATH }/${ jsonManager.getSourcePath() }`
		}).catch(() => null);
		console.error(`Failed to add: ${ name }:"${ url }"@${ version }`);
		process.exit(1);
	}
}
