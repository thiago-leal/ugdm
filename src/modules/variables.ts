import { jsonManager } from "./consts";

export function setVariable(name: string, value: string | number | boolean) {
  jsonManager.setVariable(name, value);
  jsonManager.save();
}

export function unsetVariable(name: string) {
  jsonManager.unsetVariable(name);
  jsonManager.save();
}