import { existsSync, mkdirSync } from "fs";
import { jsonManager } from "./consts";
import { installPackage } from "./install-package";
import { exec } from "./exec";
import { PATH } from "./consts";

export async function update(name: string, tag = false, branch?: string) {
  console.log(`Updating "${ name }"...`);

	const path = jsonManager.getSourcePath().split("/");

	try {
    const content = jsonManager.content.dependencies?.[name];
    const cwd = `${ PATH }/${ jsonManager.getSourcePath() }/${ name }`;

    if (content) {
      let { url, version, commands, run, auth } = content;
      let mkdirPath = "";

      for (let i = 0; i < path.length; i++) {
        mkdirPath += `${ path[i] }/`;
        if (!existsSync(mkdirPath)) {
          mkdirSync(mkdirPath);
        }
      }
      if (tag) {
        let cmd = `git rev-list --max-count=1`;

        await exec("git fetch -tap", { cwd });

        if (branch) {
          await exec(`git switch ${ branch } && git pull --tags`, { cwd });
          cmd = `git rev-list --branches=*${ branch } --max-count=1`;
        } else {
          await exec("git checkout HEAD", { cwd });
        }
        version = await exec(`git describe --tags --abbrev=0 $(${ cmd })`, { cwd })
          .then(e => e.stdout.toString().trim())
          .catch(() => version);
      } else if (branch) {
        version = branch;
      }
      const commit = await installPackage({ name, url, version, commands, run });

      jsonManager.setDependency({
        name, url, version, commands, run, auth,
        commit: tag || commit === version ? void 0 : commit
      });
      jsonManager.save();
      console.log(`Package updated: ${ name }:"${ url }"@${ version }`);
    }
	} catch (e) {
    console.error(e);
		console.error(`Failed to update dependency "${ name }"`);
		process.exit(1);
	}
}
