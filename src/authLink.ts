import { ScopeDeclaration } from "./types";

function getScopeUrl(scope: string | ScopeDeclaration): string {
  if (typeof scope === "string") return scope;
  return scope.url;
}

function getScopeAuth(scope: string | ScopeDeclaration): string | undefined {
  if (typeof scope === "string") return void 0;
  return scope.auth;
}

function getScope(
  scopes: Record<string, string | ScopeDeclaration>,
  url: string
): ScopeDeclaration {
  let auth: string | undefined;

  if (url.startsWith("@")) {
    const replace = url.split("/").at(0);

    if (replace) {
      const key = replace.slice(1);

      if (key) {
        const scope = scopes[key];

        auth = getScopeAuth(scope) ?? auth;
        url = url.replace(replace, getScopeUrl(scope));
      }
    }
  }
  return { url, auth };
}

export function authLinkGetter(
  useAuth: boolean,
  scopes: Record<string, string | ScopeDeclaration> = {}
) {
  if (useAuth) {
    return (uri: string, auth?: string) => {
      const scope = getScope(scopes, uri);

      uri = scope.url;
      auth ??= scope.auth;

      if (auth) {
        const url = new URL(uri);
        return `${ url.protocol }//${ auth }@${ url.host }${ url.pathname }`;
      }
      return uri;
    }
  } else {
    return (url: string) => getScope(scopes, url).url;
  }
}
