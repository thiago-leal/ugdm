export type DependencyDeclaration = {
  url:        string
  version:    string
  commit?:    string
  auth?:      string
  commands?:  string | Record<string, string>
  run?:       string | string[]
};

export type ScopeDeclaration = {
  url:    string
  auth?:  string
};

export type JSONFileDeclaration = {
  source:         string
  scopes?:        Record<string, string | ScopeDeclaration>
  dependencies?:  Record<string, DependencyDeclaration>
  variables?:     Record<string, string | number | boolean>
};
