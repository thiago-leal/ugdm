import { existsSync, readFileSync, writeFileSync } from "fs";
import { join } from "path";

import { jsonFileName, scriptFileName } from "./const";
import { authLinkGetter } from "./authLink";
import { jsonManager } from "./modules/consts";
import { exec } from "./modules/exec";
import { JSONFileDeclaration } from "./types";

const filename = jsonFileName;
const shFile = `${ scriptFileName }`;
const batchFile = `${ scriptFileName }.bat`;

export function getArrayFromObject<T>(obj: Record<string, T>): { key: string, value: T }[] {
  return Object.keys(obj).map(key => ({ key, value: obj[key] }));
}

function getCommands(platform: string, commands: string | Record<string, string>) {
  if (typeof commands === "string") {
    return commands;
  }
  if (typeof commands === "object") {
    return commands[platform] || "";
  }
  return "";
}

function getScriptsToRun(run: string | string[]) {
  if (Array.isArray(run)) {
    return run;
  }
  return [ run ];
}

export async function createLinux(path: string, useAuth = true, prod = false) {
  try {
    if (existsSync(path)) {
      const file = join(path, filename);

      if (existsSync(file)) {
        const json = JSON.parse(readFileSync(file).toString()) as JSONFileDeclaration;
        const path = jsonManager.getSourcePath().split("/");
        const getLink = authLinkGetter(useAuth, json.scopes);
        let mkdirPath = "";

        const mkdir = path.map((_, i) => {
          mkdirPath += `${ path[i] }/`;
          return path[i] ? `mkdir -p ${ mkdirPath }` : "";
        }).filter(e => e);

        const content = `
          #!/bin/sh

          ${ prod ? "set -e" : "" }

          ${
            json.variables && Object.keys(json.variables).length
              ? Object.keys(json.variables)
                .map(key => `${ key }=${ json.variables?.[key] }`)
                .join("\n")
              : ""
          }

          ${ mkdir.join("\n") }
          cd ${ json.source }

          ${
            getArrayFromObject(json.dependencies || {}).map(({ value, key }) => `
              if [ -d "${ key }" ]; then
                {tab}git -C ${ key } fetch -tap
                {tab}git -C ${ key } checkout ${ value.commit || value.version }
              else
                {tab}git clone ${ prod && !value.commit ? "--depth=1 " : "" }-b ${ value.version } ${ getLink(value.url, value.auth) } ${ key }
                ${ value.commit ? `{tab}git -C ${ key } checkout ${ value.commit }` : "" }
              fi
              ${
                value.run
                  ? `
                    cd ${ key }
                    ${ getScriptsToRun(value.run).map(e => `sh ${ e }`).join("\n") }
                    cd ..
                  `
                  : ""
              }
              ${
                value.commands
                  ? `
                    cd ${ key }
                    ${ getCommands("linux", value.commands) }
                    cd ..
                  `
                  : ""
              }
              `.trim()).filter(e => !!e).join("\n\n")
          }
          cd ${ mkdir.map(_ => "..").join("/") }
        `.trim()
          .replace(/\s+$/gm, "")
          .replace(/^\s+/gm, "")
          .replace(/{tab}/gm, "\t")
          .replace(/\{\{\s*([a-z0-9]+)\s*\}\}/gmi, "$$$1");

        writeFileSync(shFile, content);

        if (process.platform === "linux") {
          await exec(`chmod +x ${ shFile }`);
        }
      }
    }
  } catch (e) {
    console.error("Shell script generation failed...");
    throw e;
  }
}

export function createWindows(path: string, useAuth = true, prod = false) {
  try {
    if (existsSync(path)) {
      const file = join(path, filename);

      if (existsSync(file)) {
        const json = JSON.parse(readFileSync(file).toString()) as JSONFileDeclaration;
        const path = jsonManager.getSourcePath().split("/");
        const getLink = authLinkGetter(useAuth, json.scopes);
        let mkdirPath = "";

        const mkdir = path.map((_, i) => {
          mkdirPath += `${ path[i] }`;
          const ret = path[i] ? `IF NOT EXIST ${ mkdirPath } @MKDIR ${ mkdirPath }` : "";
          mkdirPath += "\\";
          return ret;
        }).filter(e => e);

        const content = `
          echo off

          ${
            json.variables && Object.keys(json.variables).length
              ? Object.keys(json.variables)
                .map(key => `set ${ key }=${ json.variables?.[key] }`)
                .join("\n")
              : ""
          }

          ${ mkdir.join("\n") }
          CD "${ json.source.split("/").map(e => e.replace(/\\/g, "")).join("\\") }"

          ${
            getArrayFromObject(json.dependencies || {}).map(({ value, key }) => `
              IF EXIST ${ key } (
                {tab}CALL git -C ${ key } fetch -tap
                {tab}CALL git -C ${ key } checkout ${ value.commit || value.version }
              ) ELSE (
                {tab}CALL git clone ${ prod && !value.commit ? "--depth=1 " : "" }-b ${ value.version } ${ getLink(value.url, value.auth) } ${ key }
                ${ value.commit ? `{tab}CALL git -C ${ key } checkout ${ value.commit }` : "" }
              )
              ${
                value.run
                  ? `
                    CD ${ key }
                    ${ getScriptsToRun(value.run).map(e => `CALL ${ e }`).join("\n") }
                    CD ..
                  `
                  : ""
              }
              ${
                value.commands
                  ? `
                    CD ${ key }
                    ${ getCommands("win32", value.commands) }
                    CD ..
                  `
                  : ""
              }
              `.trim()).filter(e => !!e).join("\n\n")
            }
            CD ${ mkdir.map(_ => "..").join("\\") }
        `.trim()
        .replace(/\s+$/gm, "")
        .replace(/^\s+/gm, "")
        .replace(/{tab}/gm, "\t")
        .replace(/\{\{\s*([0-9]+)\s*\}\}/gmi, "%$1")
        .replace(/\{\{\s*([$a-z]([a-z0-9])*)\s*\}\}/gmi, "%$1%");

        writeFileSync(batchFile, content);
      }
    }
  } catch (e) {
    console.error("Batch script generation failed...");
    throw e;
  }
}
