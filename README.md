# UGDM: Universal GIT Dependency Manager
UGDM is a dependency manager that uses only GIT to install packages, independently of the language

### Installing

#### Via NPM
```shell
npm i -g ugdm
```

#### Via GIT Repository
```shell
git clone https://gitlab.com/thiagofleal/ugdm.git
cd ugdm
npm run build
npm link ugdm
```

### Check if UGDM is installed
Use terminal/command prompt:
```shell
ugdm --version
```

### Using
Avaliable commands:
- **ugdm init**
Initialize GIT dependencies

- **ugdm add *[name]***
Add GIT dependency

- **ugdm remove *[name]***
Remove GIT dependency

- **ugdm update *[name]***
Update GIT dependency version

- **ugdm install**
Install GIT dependencies

- **ugdm build**
Generate shell script to install dependencies

- **ugdm set *[var]* *[value]***
Set a script variable

- **ugdm unset *[var]***
Unset a script variable

- **ugdm scope *[name]* *[url]***
Set a scope name

- **ugdm unscope *[name]***
Unset a scope name
